use cpython::{PyErr, Python};
use pyruvate;

#[test]
fn create_sync_logger() {
    let gil = Python::acquire_gil();
    let py = gil.python();
    match pyruvate::sync_logger(py, "foo") {
        Ok(()) => (),
        _ => assert!(false)
    }
    // can't initialize logger twice
    match pyruvate::sync_logger(py, "foo") {
        Err(mut e) => {
            assert!(e.instance(py).get_type(py).name(py) == "ValueError");
            PyErr::fetch(py);
        },
        _ => assert!(false)
    }
}
