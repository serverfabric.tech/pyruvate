WSGI Benchmarks
===============

Benchmarking code is heavily based on https://github.com/omedhabib/WSGI_Benchmarks.

Introduction
------------

The current setup is based on a quad core machine:

  * 2 cores are dedicated to docker / WSGI server
  * 2 cores are dedicated to web server stress tester

Steps to reproduce benchmarks
-----------------------------

Podman is required to run the benchmarking script.

Run benchmark.sh as a user that has docker permissions (it will automatically create the image), passing in directories to store results

.. code-block::

    for directory in round*; do
        ./benchmark.sh $directory
    done

Results.py will parse the results, producing a CSV file. Pass in the directories used in the previous step

.. code-block::

    ./results.py round* > results.csv
